from project import app
from flask import render_template
from project.helpers.bd_helper import is_one_user


@app.route('/')
def index():
    return render_template('index.html', is_one_user=is_one_user())


@app.route('/about')
def about():
    return render_template('about.html', is_one_user=is_one_user())


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404



