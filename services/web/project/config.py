import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL", "sqlite://")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    REDIS_HOST = 'redis'
    REDIS_PORT = 6379
    REDIS_DB = 0
    SECRET_KEY = 'super secret key'
    SECURITY_PASSWORD_SALT = 'salt'
    SECURITY_PASSWORD_HASH = 'bcrypt'
    SECURITY_REGISTERABLE = True
    SECURITY_REGISTER_URL = '/register'
    SECURITY_POST_LOGIN_VIEW = '/messages'
    SECURITY_POST_REGISTER_VIEW = '/messages'
    SECURITY_SEND_REGISTER_EMAIL = False
    MESSAGE_EXPIRED = 5
