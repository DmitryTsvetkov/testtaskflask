import logging.config
from os import path
from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger
from project.config import Config
from project.messages.view import messages
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_security import SQLAlchemyUserDatastore, Security

log_file_path = path.join(path.dirname(path.abspath(__file__)), 'log.conf')
logging.config.fileConfig(log_file_path)
log = logging.getLogger('main')

try:
    # Init app
    app = Flask(__name__)
    app.config.from_object(Config)

    # Connecting a database and migrations
    db = SQLAlchemy(app)
    migrate = Migrate(app, db)
    manager = Manager(app)
    manager.add_command('db', MigrateCommand)

    # ADD Security Module
    from project.models import *
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security = Security(app, user_datastore)

    # Register a module for working with messages
    app.register_blueprint(messages, url_prefix='/messages')

    # Initialization of information api
    from project.messages.api import Info
    api = swagger.docs(Api(app), apiVersion='0', produces=["application/json"])
    api.add_resource(Info, '/api/info/')
except Exception as e:
    log.exception('\n500\nStart server error: ' + str(e))
