from flask_restful import Resource
from project.helpers.bd_helper import is_one_user


class Info (Resource):
    def get(self):
        """
        Return True if there is one user in the database else False
        """
        return is_one_user()
