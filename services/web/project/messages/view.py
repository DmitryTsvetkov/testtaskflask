import redis
import time
from os import path
import logging.config
from datetime import timedelta
from flask import Blueprint
from flask import render_template, request, url_for, redirect
from project.config import Config
from flask_security import login_required

log_file_path = path.join(path.dirname(path.abspath(__file__)), '../log.conf')
logging.config.fileConfig(log_file_path)
log = logging.getLogger('main')

messages = Blueprint('messages', __name__, template_folder='templates')
r = redis.Redis(host=Config.REDIS_HOST, port=Config.REDIS_PORT, db=Config.REDIS_DB)

try:
    @messages.route('/', methods=['POST', 'GET'])
    @login_required
    def show():
        """
        Show messages page

        if Post method and form_name = delete - delete message

        return render messages page
        """
        from project.helpers.bd_helper import is_one_user
        if request.method == 'POST':
            if request.form.get('delete'):
                r.delete(request.form.get('delete'))
                return redirect(url_for('messages.show'))

        cursor, keys_list = r.scan()
        messages_list = r.mget(keys_list)
        data = list()
        for i in range(len(keys_list)):
            item = dict()
            item['id'] = keys_list[i].decode("utf-8")
            item['value'] = messages_list[i].decode("utf-8")
            data.append(item)
        content = dict()
        content['messages'] = sorted(data, reverse=True, key=lambda elem: elem['id'])
        content['is_one_user'] = is_one_user()
        return render_template('messages/show.html', content=content)


    @messages.route('/create', methods=['POST', 'GET'])
    @login_required
    def create():
        """
                Show message create page

                if Post method and form_name = text - create new message

                return render message create page
                """
        if request.method == 'POST':
            if request.form.get('text'):
                r.setex(str(round(time.time() * 1000)), timedelta(minutes=Config.MESSAGE_EXPIRED), request.form.get('text'))
                return redirect(url_for('messages.create'))
        cursor, keys_list = r.scan()
        return render_template('messages/create.html', count_messages=len(keys_list))
except Exception as e:
    log.exception('\n500\nmessages/view.py error: ' + str(e))
