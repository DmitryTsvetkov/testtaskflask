from flask import jsonify


def response_builder(data, message='', status_code=200):
    if message:
        data['message'] = message
    resp = jsonify(data)
    if status_code != 200:
        resp.status_code = status_code
    return resp
